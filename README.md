# steganography_python_example

Small steganography example for a series of articles on [my blog](https://tpegler.hashnode.dev/).

Requires the `Pillow` image processing library. Install with your favourite package manager. i.e. `pip install Pillow`

Then add a PNG file and run:

`python main.py encode {filepath}` or `python main.py decode {filepath}`