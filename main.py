import argparse

from PIL import Image


def encode(filepath):
    start = '#####'
    stop = '*****'
    full = start + 'Some string that you want to encode into an image' + stop

    binary_text = ''.join('{0:08b}'.format(ord(x), 'b') for x in full)
    print(binary_text, len(binary_text))

    with Image.open(filepath) as im:
        i = 0
        w, h = im.size

        for x in range(0, w):
            for y in range(0, h):
                if i >= len(binary_text):
                    i = 0

                bit = binary_text[i]
                pixel = im.getpixel((x, y))

                if bit == "0":
                    # Is odd, should be even.
                    if pixel[0] % 2 != 0:
                        new_pix = (pixel[0] - 1, pixel[1], pixel[2])
                        im.putpixel((x, y), new_pix)
                else:
                    # Is even, should be odd.
                    if pixel[0] % 2 == 0:
                        new_pix = (pixel[0] - 1, pixel[1], pixel[2])
                        im.putpixel((x, y), new_pix)

                i += 1

        im.save("3_enc.png")


def decode(filepath):
    start = '#####'
    stop = '*****'
    binary_stop = ''.join('{0:08b}'.format(ord(x), 'b') for x in stop)

    with Image.open(filepath) as im:
        w, h = im.size
        binary_text = ''

        for x in range(0, w):
            for y in range(0, h):
                pixel = im.getpixel((x, y))

                if binary_text.endswith(binary_stop):
                    message = "".join([chr(int("".join(binary_text[i:i + 8]), 2)) for i in
                                       range(0, len(binary_text) - 8, 8)])

                    start_point = message.find(start) + len(start)
                    end = message.find(stop)
                    message = message[start_point:end]
                    return message

                if pixel[0] % 2 != 0:
                    binary_text += '1'
                else:
                    binary_text += '0'


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--action", help="encode or decode")
    parser.add_argument("-f", "--filepath", help="path to image")
    args = parser.parse_args()

    if args.filepath:
        if args.action == "encode":
            encode(args.filepath)
        elif args.action == "decode":
            print(decode(args.filepath))
        else:
            print("Invalid action")
    else:
        print("No filepath provided")
